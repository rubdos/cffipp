# Copyright 2016 -  Ruben De Smet
#
# This file is part of CFFIpp.
#
# CFFIpp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CFFIpp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CFFIpp.  If not, see <http://www.gnu.org/licenses/>.

import imp

from cffipp import FFIpp

from .test_base import TestBase, subdir_path


class SmokeTests(TestBase):

    def basic_checks(self, subdirectory=False):
        example_module = self.do_import(subdirectory)
        c = example_module.testClass()
        self.assertEqual(c.returnsInt(5), 1)

    def setUp(self):
        super().setUp("smoke_test.hpp", "smoke_test.cpp", "example_module")

    def do_import(self, subdir=False):
        if subdir:
            from subdirectory import example_module
        else:
            import example_module
        imp.reload(example_module)
        return example_module

    def test_basic_compilation(self):
        self.assertTrue(self.ffi.has_class("testClass"))
        self.assertTrue(self.ffi.classes["testClass"].has_method("test"))
        self.assertFalse(
            self.ffi.get_class("testClass").has_public_method("shouldNotBeInModule"))

        self.ffi.compile(verbose=True)

        self.basic_checks()

        example_module = self.do_import()
        self.assertFalse(issubclass(
            example_module.testClass, example_module.baseClass))

    def test_basic_inherited_compilation(self):
        self.ffi.get_class("testClass").set_python_parent("baseClass")
        self.ffi.compile()

        self.basic_checks()

        example_module = self.do_import()
        self.assertTrue(issubclass(
            example_module.testClass, example_module.baseClass))

    def test_subdirectory_compilation(self):
        self.target_directory = subdir_path
        self.ffi.compile(target=subdir_path, verbose=True)

        self.basic_checks(True)

    def test_custom_imports(self):
        self.ffi.get_class("testClass").add_python_import("cffi")
        self.ffi.get_class("testClass").set_python_parent("cffi.FFI")
        self.ffi.compile()

        self.basic_checks()
