# Copyright 2016 -  Ruben De Smet
#
# This file is part of CFFIpp.
#
# CFFIpp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CFFIpp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CFFIpp.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import os
import imp

from cffipp import FFIpp
from .test_base import TestBase


class TypeTransformationTests(TestBase):

    def setUp(self):
        super().setUp("transformations.hpp", "transformations.cpp", "transform_tests")

    def do_import(self):
        import transform_tests
        imp.reload(transform_tests)
        return transform_tests

    def testCStringTransformation(self):
        self.ffi.compile()

        m = self.do_import()
        instance = m.CStringTransformationTests()
        self.assertEqual(instance.returnsString(), "Hello world as function result")
        self.assertEqual(m.CStringTransformationTests.isString, "Hello world as property")

    def testBooleanTransformation(self):
        self.ffi.compile()

        m = self.do_import()
        instance = m.BooleanTransformationTests()
        self.assertEqual(instance.returnsTrue(), True)
        self.assertEqual(instance.returnsFalse(), False)
