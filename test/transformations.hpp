class CStringTransformationTests {
public:
    CStringTransformationTests();
    const char * returnsString();
    static const char * isString;
};

class BooleanTransformationTests {
public:
    BooleanTransformationTests();
    bool returnsTrue();
    bool returnsFalse();
};
