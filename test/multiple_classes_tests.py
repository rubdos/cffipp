# Copyright 2016 -  Ruben De Smet
#
# This file is part of CFFIpp.
#
# CFFIpp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CFFIpp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CFFIpp.  If not, see <http://www.gnu.org/licenses/>.


from cffipp import FFIpp

from .test_base import TestBase


class MultiClassTests(TestBase):

    def setUp(self):
        super().setUp("multiple_classes.hpp", "multiple_classes.cpp", "multiple_classes")

    def test_compile_multple_classes(self):
        self.ffi.compile()

        import multiple_classes

        self.assertEqual(multiple_classes.A().getInt(), 23)
        self.assertEqual(multiple_classes.B().getInt(), 4)
