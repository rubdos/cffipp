import os
import unittest

from cffipp import FFIpp

subdir_path = "subdirectory"


class TestBase(unittest.TestCase):

    def tearDown(self):
        files_to_remove = [
                           "__init__.py",
                           "{}.py",
                           "_{}.cpython-34m-x86_64-linux-gnu.so",
                           "_{}.cpython-35m-x86_64-linux-gnu.so",
                           "_{}.o",
                           "_{}.cpp"]
        files_to_remove = list(
            map(lambda x: x.format(self.module_name), files_to_remove))

        # Hi optimizer. Don't remove the list cast, swaps as hell.
        files_to_remove.extend(
            list(map(lambda x: os.path.join(subdir_path, x), files_to_remove)))
        for filename in files_to_remove:
            try:
                os.remove(filename)
            except OSError:
                pass

    def setUp(self, headername, sourcename, modulename):
        with open("test/" + headername, 'r') as f:
            self.header_source = f.read()
        with open("test/" + sourcename, 'r') as f:
            self.source = f.read()

        self.module_name = modulename
        ffi = FFIpp()
        ffi.cdef(self.header_source)
        ffi.set_source(modulename, self.source,
                       include_dirs=["test"])

        self.ffi = ffi
