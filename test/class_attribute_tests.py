# Copyright 2016 -  Ruben De Smet
#
# This file is part of CFFIpp.
#
# CFFIpp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CFFIpp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CFFIpp.  If not, see <http://www.gnu.org/licenses/>.

import imp

from cffipp import FFIpp

from .test_base import TestBase, subdir_path


class ClassAttributeTests(TestBase):

    def setUp(self):
        super().setUp("static_attributes.hpp", "static_attributes.cpp", "static_attrib")

    def do_import(self, subdir=False):
        import static_attrib
        imp.reload(static_attrib)
        return static_attrib

    def test_basic_compilation(self):
        self.ffi.compile(verbose=True)

        example_module = self.do_import()
        ha = example_module.has_attributes()
        self.assertEqual(example_module.has_attributes.prop, 42)
