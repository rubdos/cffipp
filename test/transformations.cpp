#include "transformations.hpp"

CStringTransformationTests::CStringTransformationTests() {}

const char * CStringTransformationTests::returnsString() {
    return "Hello world as function result";
}

const char * CStringTransformationTests::isString = "Hello world as property";


BooleanTransformationTests::BooleanTransformationTests()
{
}

bool BooleanTransformationTests::returnsTrue()
{
    return true;
}

bool BooleanTransformationTests::returnsFalse()
{
    return false;
}
