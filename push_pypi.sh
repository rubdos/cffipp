echo "[pypirc]
servers = pypi
[server-login]
username:$PYPI_USER
password:$PYPI_PASSWORD" > ~/.pypirc
pip install wheel twine
python setup.py sdist upload
