# Copyright 2016 -  Ruben De Smet
#
# This file is part of CFFIpp.
#
# CFFIpp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CFFIpp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CFFIpp.  If not, see <http://www.gnu.org/licenses/>.


class TypeTransformationMeta(type):
    def __getattr__(cls, name):
        if name == "cpp_types":
            print("cpp_types called")
            return [cls.cpp_type]
        raise AttributeError

class TypeTransformation(object, metaclass=TypeTransformationMeta):

    @staticmethod
    def python_transform(x, type):
        for cls in TypeTransformation.__subclasses__():
            if type in cls.cpp_types:
                try:
                    transformed = cls().transform_python(x)
                    return transformed
                except AttributeError:
                    pass
        return x
    @staticmethod
    def cpp_transform(x, type):
        for cls in TypeTransformation.__subclasses__():
            print(type)
            if type in cls.cpp_types:
                try:
                    transformed = cls().transform_cpp(x)
                    return transformed
                except AttributeError:
                    pass
        return (type, x)
